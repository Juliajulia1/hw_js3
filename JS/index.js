let num = +prompt('Please write number');
while (isNaN(num) || !Number.isInteger(num)) {
    alert('Error');
    num = +prompt('Please write number');
}
if (num>=5) {
    for (let i = 0; i < num; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log("Sorry, no numbers");
}